
/* Fonctions de transformations des données */

/**
 * Transforme un tableau en objet
 * 
 * @param {*} arr 
 */
function toObject(arr) {
    let rv = {};
    for (const key in arr) {
        if (arr.hasOwnProperty(key)) {
            rv[key] = arr[key];
        }
    }
    return rv;
}

/**
 * Transforme un objet en tableau
 * 
 * @param {*} obj 
 */
function toArray(obj) {
    let rv = [];
    for (const key in obj) {
        if (obj.hasOwnProperty(key)) {
            rv[key] = obj[key];
        }
    }
    return rv;
}

/**
 * Dans un string, permet de retourner si il contient le nom d'une region ou pas
 * Si oui, alors il retourne le nom de la region, sinon il retourne 'erreur'
 * 
 * @param {*} libelle 
 */
function detectRegion(libelle) {
    let resultat = "erreur";
    nomsRegions.forEach(nom => {
        if (libelle.search(nom) != -1) {
            resultat = nom;
        }
    });
    return resultat;
}

/**
 * Dans un string, permet de retourner si il contient le nom d'un secteur ou pas
 * Si oui, alors il retourne le nom du secteur, sinon il l'ajoute dans une liste des noms des secteurs
 * 
 * @param {*} libelle 
 */
function detectSecteurOuAjouteSecteur(libelle) {
    let resultat = "erreur";
    if (nomsSecteurs.indexOf(libelle) == -1) {
        nomsSecteurs.push(libelle);
        resultat = libelle;
    } else {
        resultat = libelle;
    }
    return resultat;
}

/**
 * Permet de charger les donnees JSON avec des requetes asynchrones
 * 
 * @param {*} cheminDuFichier 
 * @param {*} fonctionAAppellerALaFin 
 */
function chargerFichierJSON(cheminDuFichier, fonctionAAppellerALaFin) {
    let xhr = new XMLHttpRequest();
    xhr.overrideMimeType("application/json");
    xhr.onload = function () {
        return fonctionAAppellerALaFin(this.responseText)
    }
    xhr.open("GET", cheminDuFichier, true);
    xhr.send();
}

/**
 * Permet de transformer une donnee pour faciliter le traitement
 * 
 * @param {*} cle La clé permettant d'identifier la donnée ('emplois', 'pib', 'pibHabitant' et 'tauxChomage')
 */
function transformerDonneesJSON(cle) {
    let donnees = null;
    const donneesTransformees = [];
    let tabDonneesEntier;
    let nomRegion;
    let nomSecteur;
    switch (cle) {
        // case 'emplois':
        //     donnees = donneesJSON[cle].data;
        //     donnees.forEach(tab => {
        //         tabDonneesEntier = tab.slice(39).map(x => parseInt(x) || 0);
        //         nomRegion = detectRegion(tab[0]);
        //         if (nomRegion != "erreur") {
        //             if (!donneesTransformees[nomRegion]) {
        //                 donneesTransformees[nomRegion] = tabDonneesEntier;
        //             } else {
        //                 for (let index = 0; index < tabDonneesEntier.length; index++) {
        //                     donneesTransformees[nomRegion][index] += tabDonneesEntier[index];
        //                 }
        //             }
        //         }
        //     });
        //     donneesTransformees["annee"] = donnees[0].slice(39);
        //     donnees = donneesTransformees;
        //     break;

        case 'emplois':
            donnees = donneesJSON[cle].data;
            donnees.forEach((tab, i) => {
                if (i !== 0) {
                    tabDonneesEntier = tab.slice(3).map(x => parseInt(x) || null);
                    nomRegion = detectRegion(tab[0]);
                    nomSecteur = detectSecteurOuAjouteSecteur(tab[0].split(' - ')[1]);
                    if (nomRegion != "erreur" && nomSecteur != "erreur") {
                        if (listeNoirNomsSecteurs.indexOf(nomSecteur) == -1) {
                            if (!donneesTransformees[nomRegion]) {
                                donneesTransformees[nomRegion] = [];
                                if (!donneesTransformees[nomRegion][nomSecteur]) {
                                    donneesTransformees[nomRegion][nomSecteur] = tabDonneesEntier;
                                }
                            } else {
                                if (!donneesTransformees[nomRegion][nomSecteur]) {
                                    donneesTransformees[nomRegion][nomSecteur] = tabDonneesEntier;
                                }
                            }
                        } else if (nomSecteur === "Ensemble des salariés") {
                            if (!donneesTransformees[nomRegion]) {
                                donneesTransformees[nomRegion] = [];
                                if (!donneesTransformees[nomRegion]['all']) {
                                    donneesTransformees[nomRegion]['all'] = tabDonneesEntier;
                                }
                            } else {
                                if (!donneesTransformees[nomRegion]['all']) {
                                    donneesTransformees[nomRegion]['all'] = tabDonneesEntier;
                                } 
                            }
                        }
                    }
                    
                }
            });
            donneesTransformees["annee"] = donnees[0].slice(3);
            donnees = donneesTransformees;
            break;


        case 'pib':
            donnees = donneesJSON[cle].data;
            donnees.forEach(tab => {
                tabDonneesEntier = tab.slice(1).map(x => parseFloat(x.replace(',', '.').replace(' ', '')) || 0);
                nomRegion = detectRegion(tab[0]);
                if (nomRegion != "erreur") {
                    if (!donneesTransformees[nomRegion]) {
                        donneesTransformees[nomRegion] = tabDonneesEntier;
                    }
                }
            });
            donneesTransformees["annee"] = donnees[3].slice(1)
            donnees = donneesTransformees;
            break;

        case 'pibHabitant':
            donnees = donneesJSON[cle].data;
            donnees.forEach(tab => {
                tabDonneesEntier = tab.slice(1).map(x => parseFloat(x.replace(',', '.').replace(' ', '')) || 0);
                nomRegion = detectRegion(tab[0]);
                if (nomRegion != "erreur") {
                    if (!donneesTransformees[nomRegion]) {
                        donneesTransformees[nomRegion] = tabDonneesEntier;
                    }
                }
            });
            donneesTransformees["annee"] = donnees[3].slice(1)
            donnees = donneesTransformees;
            break;

        case 'tauxChomage':
            donnees = donneesJSON[cle].data;
            donnees.forEach(tab => {
                tabDonneesEntier = tab.slice(3).map(x => parseFloat(x) || 0);
                nomRegion = detectRegion(tab[0]);
                if (nomRegion != "erreur") {
                    if (!donneesTransformees[nomRegion]) {
                        donneesTransformees[nomRegion] = tabDonneesEntier;
                    } else {
                        for (let index = 0; index < tabDonneesEntier.length; index++) {
                            donneesTransformees[nomRegion][index] += tabDonneesEntier[index];
                        }
                    }
                }
            });
            donneesTransformees["annee"] = donnees[0].slice(3);
            donnees = donneesTransformees;
            break;

        case 'regions':
            // Ne rien faire
            donnees = donneesJSON[cle];
            break;

        default:
            console.log('Ce fichier n\'est pas supporté et ne peut pas être transformé.');
            break;
    }
    return donnees;
}

/**
 * Fonction permettant d'assigner une année à chaque valeurs, par exemple :
 * 
 * donneesJSON['pib']['Bretagne'] = [123, 456, 789]
 * donneesJSON['pib']['annee'] = [2016, 2017, 2018]
 * 
 * Se transforme en :
 * 
 * donneesJSON['pib']['Bretagne'] = {
 *  2016: 123, 
 *  2017: 456, 
 *  2018: 789
 * }
 * 
 * @param cle La clé permettant d'identifier la donnée ('emplois', 'pib', 'pibHabitant' et 'tauxChomage')
 */
function associerAnneeEtDonnees(cle) {
    if (cle != 'emplois') {
        let donneesAssociees;
        for (const region in donneesJSON[cle]) {
            donneesAssociees = [];
            if (donneesJSON[cle].hasOwnProperty(region)) {
                for (let index = 0; index < Object.keys(donneesJSON[cle]["annee"]).length; index++) {
                    const annee = donneesJSON[cle]["annee"][index];
                    donneesAssociees['annee-' + annee] = donneesJSON[cle][region][index];
                }
                donneesJSON[cle][region] = toObject(donneesAssociees);
            }
        }
    } else {
        let donneesAssociees;
        for (const region in donneesJSON[cle]) {
            donneesAssociees = [];
            if (donneesJSON[cle].hasOwnProperty(region)) {
                for (const secteur in donneesJSON[cle][region]) {
                    if (donneesJSON[cle][region].hasOwnProperty(secteur)) {
                        for (let index = 0; index < Object.keys(donneesJSON[cle]["annee"]).length; index++) {
                            const annee = donneesJSON[cle]["annee"][index];
                            donneesAssociees['annee-' + annee] = donneesJSON[cle][region][secteur][index];
                        }
                        donneesJSON[cle][region][secteur] = toObject(donneesAssociees);
                    }
                }
            }
        }
    }
    delete donneesJSON[cle]["annee"];
}

/* Fin Fonctions de transformations des données */