/**
 * Initialise le select de secteur d'activite
 */
let selectSecteur = document.getElementById('select-secteur');

selectSecteur.addEventListener('change', event => {
    secteurCourant = event.target.value;
    updateCharts(regionCourante, secteurCourant);
});

let selectSecteurOptionList = document.getElementById('select-secteur').options;